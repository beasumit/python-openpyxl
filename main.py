import openpyxl

main_file = openpyxl.load_workbook("inventory.xlsx")
product_list = main_file["Sheet1"]

product_per_supplier = {}
total_value_per_supllier = {}
product_under_10_val = {}

for product_row in range(2, product_list.max_row + 1):
    supplier_name = product_list.cell(product_row, 4).value
    inventory = product_list.cell(product_row, 2).value
    price = product_list.cell(product_row, 3).value
    product_num = product_list.cell(product_row, 1).value
    inventory_price = product_list.cell(product_row, 5)

    # calculation_for_num_of_supplier
    if supplier_name in product_per_supplier:
        current_num_product = product_per_supplier.get(supplier_name)
        product_per_supplier[supplier_name] = current_num_product + 1
    else:
        product_per_supplier[supplier_name] = 1
    # calculate_total_value_of_per_supplier
    if supplier_name in total_value_per_supllier:
        current_total_value = total_value_per_supllier.get(supplier_name)
        total_value_per_supllier[supplier_name] = current_total_value + inventory * price
    else:
        total_value_per_supllier[supplier_name] = inventory * price
#   calculation for product that is less than the value 10
    if inventory < 10:
        product_under_10_val[int(product_num)] = int(inventory)

#   Saving the value of Price and Quantity in new Column
    inventory_price.value = inventory * price

print(product_per_supplier)
print(total_value_per_supllier)
print(product_under_10_val)
main_file.save("inventory_with_total_value.xlsx")
